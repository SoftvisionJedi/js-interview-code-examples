let val1 = 0;
let val2 = 0;
let len = 0;

function fibonacci(a, b, l) {
	const result = [a, b];

	if(l < result.length) return -1;

  for(let i = 2; i <= l; i++) result.push(result[i - 1] + result[i - 2])
	
	console.log(result);
  return result[l]
}

val1 = 5;
val2 = 7;
len = 12;
console.log(`The final value for ${val1} and ${val2} is ${fibonacci(val1, val2, len)}`);