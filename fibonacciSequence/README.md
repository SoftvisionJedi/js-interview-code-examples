# Fibonacci Challenge

A Fibonacci sequence is an ordering of numbers where each number is the sum of the preceding two. 

For example, the first ten numbers of the Fibonacci sequence are: 0, 1, 1, 2, 3, 5, 8, 13, 21, 34.
