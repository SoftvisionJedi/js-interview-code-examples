let uri = '';

function queryToObject(str){
	const obj = {};
	if(str.indexOf('?') > -1) {
		let arr = str.split('?');
		if(arr[1].length > 0) {
			obj.queryStr = `?${arr[1]}`;
			arr = arr[1].split('&');

			arr.forEach(function(pair) {
        pair = pair.split('=');
        obj[pair[0]] = decodeURIComponent(pair[1] || '');
    	});
		}
	}
	return obj;

}

function objectToQuery(obj){

}

uri = 'http://www.test.com/findUser?firstName=John&lastName=Smith';
console.log(queryToObject(uri));

uri = 'http://www.test.com/findUser?firstName=John';
console.log(queryToObject(uri));

uri = 'http://www.test.com/findUser';
console.log(queryToObject(uri));