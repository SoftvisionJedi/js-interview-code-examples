const string = 'Now let me tell you a story about a man name Jed. The poor mountaineer kept his family barely fed. Then one day, he was shooting at some food';

function firstNonRepeatedCharacter(val) {
	const str = val.toLowerCase();
  for (var i = 0; i < str.length; i++) {
    var c = str.charAt(i);
    if (str.indexOf(c) === i && str.indexOf(c, i + 1) === -1) {
      return {
				char: c,
				idx: i
			};
    }
  }
  return null;
}

const r = firstNonRepeatedCharacter(string);
console.log(`The first unique character found is '${r.char}' @ index ${r.idx}`);