let val1 = '';
let val2 = '';

function build(str) {
	const charObj = {};
	for (let char of str.replace(/[^\w]/g).toLowerCase()) {
		charObj[char] = charObj[char] + 1 || 1;
	}
	return charObj;
}


function testForAnagram(strA, strB) {
	const a = build(strA);
	const b = build(strB);

	if (Object.keys(a).length !== Object.keys(b).length) {
		return false;
	}

	for (let c in a) if (a[c] !== b[c]) return false;

	return true;
}

val1 = 'finder';
val2 = 'friend';

console.log(`Is '${val1}' an anagram of '${val2}': ${testForAnagram(val1, val2)}`);