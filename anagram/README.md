# Anagram Challenge

- write a function that checks if two provided strings are anagrams of each other; 
- letter casing shouldn’t matter. 
- Also, consider only characters, not spaces or punctuation