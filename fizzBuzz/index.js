
function fizzBuzz(num) {
  for(let i = 1; i <= num; i++) {
    if(i % 3 === 0 && i % 5 === 0) console.log('fizzbuzz') 	// check if the number is a multiple of 3 and 5
    else if(i % 3 === 0) console.log('fizz')								// check if the number is a multiple of 3
		else if(i % 5 === 0) console.log('buzz')								// check if the number is a multiple of 5
		else console.log(i)
  }
}

fizzBuzz(15);