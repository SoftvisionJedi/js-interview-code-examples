let val = '';

function checkPalindromeV1(str){
	for (let i = 0; i < str.length / 2; i++) {
		if (str.charAt(i) != str.charAt(str.length - i - 1)) return false;
	}
	return true;
}

function checkPalindromeV2(str){
  return str === str.split('').reverse().join('');
}
////////////////////////////////////////////////////////////////////////////////////////////////
// 
val = 'racecar';
console.log(`Is ${val} a palindrone: ${checkPalindromeV1(val)}`);

val = 'mom';
console.log(`Is ${val} a palindrone: ${checkPalindromeV1(val)}`);

val = 'Racecar';
console.log(`Is ${val} a palindrone: ${checkPalindromeV1(val)}`)

////////////////////////////////////////////////////////////////////////////////////////////////
// 
val = 'racecar';
console.log(`Is ${val} a palindrone: ${checkPalindromeV2(val)}`);

val = 'mom';
console.log(`Is ${val} a palindrone: ${checkPalindromeV2(val)}`);

val = 'Racecar';
console.log(`Is ${val} a palindrone: ${checkPalindromeV2(val)}`)