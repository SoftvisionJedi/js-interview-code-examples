# JS Interview Code Examples

A series of example coding interview questions with solutions

---

1.1. Check if a number is power of 10 (i.e.: 1000 it is, 500 is not).

---

1.2. Find second minimum from a list, without sorting it, time complexity should be O(n)

---

1.3. First unique character in a string

---

1.4. Binary search tree, implement PUT and CONTAIN methods (and Order traversal method is slightly wrong, fix it).

---

1.5. display how many times a character is repeating in a string 
- ex: 'aaaabb' => 'a4b2'
- '' => ''
- 'a' => 'a1'

---

1.6. A product is sold in a time frame. 1->n sellers are offered with a price. 

Example:
- Seller 1 has an offer from 1pm to 6pm with a price of 20
- Seller 2 has an offer from 2pm to 5pm with a price of 15
 
Return a vector with smallest offers in above time slots.
Solution:
- From 1pm to 2pm - price 20
- From 2pm to 5pm - price 15
- From 5pm to 6pm - price 20

---

1.7. Function to print an input string (e.g. "aaabbc") as "a3b2c1"

---

1.8. Function to find median of 2 array inputs (e.g. [1, 3] & [2, 4, 6] output is 3. 3 is the average median of the individual medians of the arrays (2 & 4)
 