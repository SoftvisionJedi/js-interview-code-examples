// Smallest Pair Sum In A Given Array

// Variables Required:
let arr = [];
let rsl = -1;

// Methods To Use
function calculate(a) {
	let min = Number.MAX_VALUE;
	for (let i = 0; i < a.length; i++) {
		const x = a[i];
		for (let j = (i + 1); j < a.length; j++) {
			const y = x + a[j];
			if(y < min){
				min = y;
			}
		}
	}
	return min;
}

function findThenCal(a) {
	let min1 = Number.MAX_VALUE;
	let min2 = Number.MAX_VALUE;
	for (let i = 0; i < a.length; i++) {
		if(a[i] < min1) {
			min2 = min1;
			min1 = a[i];
		} else if(a[i] < min2 && a[i] !== min1) {
			min2 = a[i];
		}
	}
	return min1 + min2;
}


arr = [76, 754, 43, 357, 352, 538, 24, 578, 28, 75, 980, 565, 1, 2, 4, 423, 12, 575, 987, 565, 325, 312, 332, 31, 86, 964, 43];
console.log(calculate(arr));
//console.log(findThenCal(arr));

arr = [346, 535, 656, 232, 67, 21, 243, 865, 43, 578, 31, 57, 3425, 5321, 442, 486, 424, 134, 563, 856, 438, 314, 876, 86, 53, 35, 175];
console.log(calculate(arr));
//console.log(findThenCal(arr));

arr = [33, 51, 60, 21, 13, 9, 10, 22];
console.log(calculate(arr));
//console.log(findThenCal(arr));