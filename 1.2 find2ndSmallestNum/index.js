const array = [2, 12333, 21, 22, 2, 1, 12, 13, 1, 10, 34, 1, 2, 353, 23, 56, 2, 54, 332, 32, 64, 2314];

function findSecondSmallestNum(a) {
	if(a.length < 2) { return -1; }

	let min1 = Number.MAX_VALUE;
	let min2 = Number.MAX_VALUE;

	for (let i = 0; i < a.length; i++) {
		if (a[i] < min1) {
			min2 = min1;
			min1 = a[i];
		} else if (a[i] < min2 && a[i] !== min1) {
			min2 = a[i];
		}

		console.log(`min1 = ${min1} || min2 = ${min2}`);
	}
	return min2;
}

const r = findSecondSmallestNum(array);
console.log(`The second smallest number is ${r}`);