function checkForPowerOf10(input) {
  while (input > 9 && input % 10 == 0) {
    input /= 10;
  }
  return input == 1;
}

function isPowerOfTen(input) {
  if (input % 10 != 0 || input == 0) {
    return false;
  }

  if (input == 10) {
    return true;
  }
  return isPowerOfTen(input/10);
}


const r = checkForPowerOf10(100000);
//const r = isPowerOfTen(100000);
console.log(r);