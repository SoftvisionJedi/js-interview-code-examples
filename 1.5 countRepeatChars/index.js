let val = '';

function countRepeatChars(str) {
	const keyVal = {};
	let result = '';
	
	for (let i = 0; i < str.length; i++) {
		if(keyVal[str[i]]) {
			keyVal[str[i]] = keyVal[str[i]] + 1;
		} else {
			keyVal[str[i]] = 1;
		}
	}
	const arr = Object.keys(keyVal);
	for(let x in arr) {
		result += `${arr[x]}${keyVal[arr[x]]}`;
	}

	return result;
}

console.log(countRepeatChars('aaaaabbccccdefffgghij'));