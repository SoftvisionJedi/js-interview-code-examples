// Smallest Subarray With Sum Greater Than The Given Value

// Variables Required:
let arr1 = [];
let val = -1;
let result = -1;

// Methods To Use
function smallestSubArrWithSumGt(arr, x) { 
	let minLen = arr.length + 1; 
	for (let a = 0; a < arr.length; a++) { 
    let crntSum = arr[a]; 
		
		if (crntSum > x) return 1;
		for (let z = a + 1; z < arr.length; z++) { 
      crntSum += arr[z]; 
			if (crntSum > x && (z - a + 1) < minLen) minLen = (z - a + 1); 
    } 
  } 
	return minLen; 
} 

function printResult(a,v,r) {
	if(r === (a.length + 1)) {
		console.log(`The operation is not possible`);
	} else {
		console.log(`The Smallest subarray with sum greater than a given value of ${v} is ${r}`);
	}
}

////////////////////////////////////////////////////////////////////////////////////////////////
// Result will be 3 [4, 45, 6]
arr1 = [1, 4, 45, 6, 10, 19]; 
val = 51;
result = smallestSubArrWithSumGt(arr1, val);
printResult(arr1, val, result);

////////////////////////////////////////////////////////////////////////////////////////////////
// Result will be 1 [10]
arr1 = [1, 10, 5, 2, 7]; 
val = 9;
result = smallestSubArrWithSumGt(arr1, val);
printResult(arr1, val, result);


////////////////////////////////////////////////////////////////////////////////////////////////
// Result will be 4 [100, 1, 0, 200]
arr1 = [1, 11, 100, 1, 0, 200, 3, 2, 1, 250]; 
val = 280;
result = smallestSubArrWithSumGt(arr1, val);
printResult(arr1, val, result);

////////////////////////////////////////////////////////////////////////////////////////////////
// Result will be "The operation is not possible"
arr1 = [1, 2, 4]; 
val = 8;
result = smallestSubArrWithSumGt(arr1, val);
printResult(arr1, val, result);